"use strict";

//Cards Test

//This Script is meant to simulate how the Database will handle
//The Spectra-TCG cards userstorage and all common actions

const MAX = 10000000;

class Util {
	constructor() {
		this.users = [];
	}

	genId(t) {
		let charSet = "abcdefghijklmnopqrstuvwxyz1234567890", id = "", i = 0;
		while (i < t) {
			id += charSet.charAt(Math.floor(Math.random() * charSet.length));
			i++;
		}
		return id;
	}

	newUser() {
		let id;
		let pass = false;
		while(!pass) {
			id = this.genId(8);
			if (this.users.indexOf(id) === -1) pass = true;
		}
		this.users.push(id);
		return id;
	}

	oldUser(not) {
		let selected = null;
		let rand;
		while(!selected) {
			rand = this.users[Math.floor(Math.random() * this.users.length)];
			if (!not || rand !== not) {
				selected = rand;
			}
		}
		return selected;
	}

	randomCard() {
		return this.genId(2);
	}

	ownedCard(u) {
		let cards = this.getCards(u);
		cards = Object.keys(cards);
		let card = cards[Math.floor(Math.random() * cards.length)];
		return card;
	}

	cardTotal(u) {
		let cards = this.getCards(u);
		return Object.keys(cards).length;
	}

	randomQuantity(max) {
		let q = Math.floor(Math.random() * max);
		return q === 0 ? 1 : q;
	}
}

class Wrapper extends Util {
	constructor() {
		super();
		this.root = "data";
		this.abstractor = null;
	}

	init(Abstractor) {
		this.abstractor = new Abstractor();
		this.abstractor.init(this.root);
		return this;
	}

	getCards(userid) {
		return this.abstractor.request(userid);
	}

	addCard(userid, cardid, quantity) {
		console.log(`${Date.now()} | Giving ${quantity} ${cardid} to ${userid}`);
		return this.abstractor.update(userid, "add", [cardid, quantity]);
	}

	takeCard(userid, cardid, quantity) {
		console.log(`${Date.now()} | Taking ${quantity} ${cardid} from ${userid}`);
		return this.abstractor.update(userid, "decrease", [cardid, quantity]);
	}

	takeAllCards(userid) {
		console.log(`${Date.now()} | Taking all cards from ${userid}`);
		return this.abstractor.update(userid, "clear", [null]);
	}

	transferCard(u1, c, u2, q) {
		console.log(`${Date.now()} | ${u1} is sending ${q} ${c} to ${u2}`);
		return this.abstractor.move(u1, [c, q], u2, [c]);
	}

	transferAll(u1, u2) {
		console.log(`${Date.now()} | ${u1} is sending all cards to ${u2}`);
		return this.abstractor.merge(u1, u2, {number: 1});
	}
}

//Actions:
//0 = new user add cards - Minor
//1 = old user add cards - Minor
//2 = old user add cards - Minor
//3 = old user add cards - Minor
//4 = old user take cards - Minor
//5 = old user takeAllCards - Major
//6 = old user transferCard - Major
//7 = old user trasnferAll - Major

function loop(a, i) {
	if (i === MAX) {
		console.log("Maximum Iterations reached, Test concluded");
		return;
	} else {
		console.log(`Iteration: ${i}`);
	}
	let b;
	if (i < 20) {
		//Only Add cards
		b = 0;
	} else if (i < 500) {
		//Only Run Minor Actions
		b = Math.floor(Math.random() * 5);
	} else {
		//Run Any Action
		b = Math.floor(Math.random() * 6);
	}

	let h;
	switch(b) {
	case 0:
		a.addCard(a.newUser(), a.randomCard(), 1);
		break;
	case 1:
	case 2:
	case 3:
		a.addCard(a.oldUser(), a.randomCard(), 1);
		break;
	case 4:
		h = a.oldUser();
		if (a.cardTotal(h) === 0) break; //User has no cards
		a.takeCard(h, a.ownedCard(h), 1);
		break;
	case 5:
		a.takeAllCards(a.oldUser());
		break;
	case 6:
		h = [a.oldUser()];
		h.push(a.oldUser(h[0]));
		if (a.cardTotal(h[0]) === 0) break; //User has no cards
		a.transferCard(h[0], a.ownedCard(h[0]), h[1], 1);
		break;
	case 7:
		h = [a.oldUser()];
		h.push(a.oldUser(h[0]));
		if (a.cardTotal(h[0]) === 0) break; //User has no cards
		a.transferAll(h[0], h[1]);
		break;
	}

	setTimeout(() => {
		loop(a, ++i)
	}, 1);
}

module.exports = {
	start: function(ab) {
		console.log("Starting Cards Test...");
		const a = new Wrapper().init(ab);
		console.log("Abstractor Initializing, Waiting for Ready...");
		a.abstractor.on("ready", () => {
			loop(a, 0);
		});
	}
}