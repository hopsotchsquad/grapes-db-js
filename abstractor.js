"use strict";

const msgpack = require("msgpack-js");
const MAX_SIZE = 256000;

class OUtil {
	constructor() {
		console.log("OUtil Constructor Called. This should never happen.");
	}

	static HexToBinary(buffer) {
		buffer = buffer.toString();
		let bytes = [], i = 0, j = buffer.length;
		for (i;i<j-1;i+=2) { bytes.push(buffer.substr(i,2),16); }
		return bytes.join("");
	}

	static CellAdd(obj, o, layers) {
		if (layers > 9) return null; //Too Much recursion
		if (!o.key) return null; //Missing Key
		if (!obj[o.key]) {
			if (!o.check(0)) return null; //Missing args
			obj[o.key] = o.shiftValue();
			return obj;
		} else if ((typeof obj[o.key] === "number" || typeof obj[o.key] === "string") && o.check(0)) {
			o.shiftValue();
			if (o.type !== "number" && o.type !== "string") return null; //Wrong data type
			obj[o.key] += o.value;
			return obj;
		} else if (typeof obj[o.key] === "object" && o.check(0,1)) {
			o.shiftKey();
			obj[o.key] = OUtil.CellAdd(obj[o.key], o, ++layers);
			return obj;
		} else {
			if (!o.check(0)) return null; //Missing args
			obj[o.key] = o.shiftValue();
			return obj;
		}
	}

	static CellDecrease(obj, o, layers) {
		if (layers > 9) return null; //Too Much recursion
		if (!o.key) return null; //Missing Key
		if (!obj[o.key]) {
			return obj;
		} else if (typeof obj[o.key] === "number" && o.check(0)) {
			o.shiftValue();
			if (o.type !== "number") return null; //Wrong data type
			obj[o.key] -= o.value;
			return obj;
		} else if (typeof obj[o.key] === "string" && o.check(0)) {
			o.shiftValue();
			if (o.type !== "string") return null; //Wrong data type
			obj[o.key].replace(o.value, "");
			return obj;
		} else if (typeof obj[o.key] === "object" && o.check(0,1)) {
			o.shiftKey();
			obj[o.key] = OUtil.CellDecrease(obj[o.key], o, ++layers);
			return obj;
		} else {
			delete obj[o.key];
			return obj;
		}
	}

	static CellReplace(obj, o, layers) {
		if (layers > 9) return null; //Too Much recursion
		if (!o.key) return null; //Missing Key
		if (obj[o.key] && typeof obj[o.key] === "object" && o.check(0,1)) {
			o.shiftKey();
			obj[o.key] = OUtil.CellReplace(obj[o.key], o, ++layers);
			return obj;
		} else {
			if (!o.check(0)) return null; //Missing args
			obj[o.key] = o.shiftValue();
			return obj;
		}
	}

	static CellDelete(obj, o, layers) {
		if (layers > 9) return null; //Too Much recursion
		if (!o.key) return null; //Missing Key
		if (obj[o.key] && typeof obj[o.key] === "object" && o.check(0)) {
			o.shiftKey();
			obj[o.key] = OUtil.CellDelete(obj[o.key], o, ++layers);
			return obj;
		} else {
			if (obj[o.key]) delete obj[o.key];
			return obj;
		}
	}

	static CellLocate(obj, o, layers) {
		if (layers > 9) return null; //Too Much recursion
		if (!o.key) return null; //Missing Key
		if (!obj[o.key]) {
			//This key doesnt exist
			return null;
		} else if (typeof obj[o.key] === "object" && o.check(0)) {
			return OUtil.CellLocate(obj[o.shiftKey()], o, ++layers);
		} else {
			if (o.check(1)) console.log("Unexpected additional input. - Unhandled");
			return obj[o.key];
		}
	}

	static CellType(obj, o, layers) {
		if (layers > 9) return null; //Too Much recursion
		if (!o.key) return null; //Missing Key
		if (!obj[o.key]) {
			return null;
		} else if (typeof obj[o.key] === "object" && o.check(0)) {
			return OUtil.CellType(obj[o.shiftKey()], o, ++layers);
		} else {
			return typeof obj[o.key];
		}
	}

	//This is called when we need to move data between
	//two seperate cells
	static move(iCell, id, o, nCell, nid, no) {
		//First we locate the data we need to move
		const iObj = iCell.data[id];
		const nObj = nCell.data[nid];
		if (!iObj) return null; //Object not found
		if (!nObj) return null; //Object not found

		const item = OUtil.locate(iObj, o.deRef(), 0);
		if (!item) return null; //Unable To Locate
		no.assign(item); //Add value to options

		//Try to add item to new location
		const results =
			OUtil.CellType(nObj, no.deRef(), 0) === "number" ?
			OUtil.CellAdd(nObj, no.deRef(), 0) :
			OUtil.CellReplace(nObj, no.deRef(), 0);

		if (!results) {
			return null; //Failed to add item
		} else {
			const finalized =
				OUtil.CellType(iObj, o.deRef(), 0) === "number" ?
				OUtil.CellDecrease(iObj, o.deRef(), 0) :
				OUtil.CellDelete(iObj, o.deRef(), 0);

			if (!finalized) {
				return null; //Failed to remove
			} else {
				return [finalized,results]; //Send updated objects back to caller
			}
		}
	}

	//No need to track recursion because we're adding no new Data
	static rMerge(nData, element, id, o) {
		if (!nData[id]) return element;
		const eType = typeof element;
		const nType = typeof nData[id];
		if (nType !== eType) {
			//Cant merge
			if (o.all === 0) {
				//Drop
				return nData[id];
			} else {
				//Replace
				return element;
			}
		} else if (eType === "number") {
			if (o.number === 0) {
				//Drop
				return nData[id];
			} else if (o.number === 1) {
				//Add
				return element + nData[id];
			} else {
				//Replace
				return element;
			}
		} else if (eType === "string") {
			if (o.string === 0) {
				//Drop
				return nData[id];
			} else if (o.string === 1) {
				//Add
				return element + nData[id];
			} else {
				//Replace
				return element;
			}
		} else if (eType === "object") {
			let merged = {};
			if (o.object === 0) {
				//Drop
				return nData[id];
			} else if (o.object === 4){
				//Replace
				return element;
			} else if (o.object === 1) {
				//Merge - Drop E mismatch
				for (const i in nData[id]) {
					if (element[i]) {
						merged[i] = OUtil.rMerge(nData[id], element[i], i, o);
					} else {
						merged[i] = nData[id][i];
					}
				}
				return merged;
			} else if (o.object === 2) {
				//Merge - Drop N mismatch
				for (const i in element) {
					if (nData[id][i]) {
						merged[i] = OUtil.rMerge(element, nData[id][i], i, o);
					} else {
						merged[i] = element[i];
					}
				}
				return merged;
			} else if (o.object === 3) {
				//Merge All
				for (const i in nData[id]) {
					if (element[i]) {
						merged[i] = OUtil.rMerge(nData[id], element[i], i, o);
						delete element[i]; //So we don't double-merge
					} else {
						merged[i] = nData[id][i];
					}
				}
				for (const i in element) {
					if (nData[id][i]) {
						merged[i] = OUtil.rMerge(element, nData[id][i], i, o);
					} else {
						merged[i] = element[i];
					}
				}
				return merged;
			}
		}
	}

	static merge(iData, nData, options = {string: 0, number: 0, object: 0, all: 0}) {
		if (!options.string) options.string = 0;
		if (!options.number) options.number = 0;
		if (!options.object) options.object = 0;
		if (!options.all) options.all = 0;

		let oData = {};
		if (options.all === 0) {
			//Drop
			for (const i in iData) {
				oData[i] = OUtil.rMerge(nData, iData[i], i, options);
				if (nData[i]) delete nData[i];
			}
			for (const i in nData) {
				oData[i] = nData[i];
			}
		} else {
			//Replace
			for (const i in nData) {
				oData[i] = OUtil.rMerge(iData, nData[i], i, options);
				if (iData[i]) delete iData[i];
			}
			for (const i in iData) {
				oData[i] = iData[i];
			}
		}
		return oData;
	}
}

class Pointer extends Array {
	constructor(...input) {
		super(...input);
		this.original = true;
		this.key = this.shift();
		this.value = null;
		this.type = null;
	}

	deRef() {
		if (!this.original) return null; //Can't deref a clone
		const clone = this;
		clone.original = null;
		return clone;
	}

	assign(...values) {
		if (!this.original) return null; //Can't assign to a clone
		for (const i of values) {
			this.push(i);
		}
	}

	shiftKey() {
		if (this.original) return null; //Can't Modify original
		this.key = this.shift();
		return this.key;
	}

	shiftValue() {
		if (this.original) return null; //Can't Modify original
		this.value = this.shift();
		this.type = typeof this.value;
		return this.value;
	}

	check(...index) {
		for (const i of index) {
			if (!this[i]) return false;
		}
		return true;
	}
}

class Cell extends require("events") {
	constructor(id, data) {
		super();
		this.id = id;
		this.data = data;
		this.index = null; //object containing entries this cell holds and their size in bytes
		this.dataSize = 0; //In Bytes
		this.changed = Object.keys(data).length === 0 ? true : false;
	}

	updateIndex() {
		//Index the data in this cell and return it to the wrapper
		if (!this.index) this.index = {};
		let entry;
		for (const i in this.data) {
			this.index[i] = {
				id: i,
				size: OUtil.HexToBinary(msgpack.encode(this.data[i])).length,
				cell: this.id
			};
		}
		this.updateSize();
		return this.index;
	}

	updateSize() {
		this.dataSize = OUtil.HexToBinary(msgpack.encode(this.data)).length;
		console.log(`CELL ${this.id} SIZE: ${this.dataSize}`);
		if (this.dataSize >= MAX_SIZE) {
			this.emit("divide");
		}
	}

	addTopLevel(id) {
		if (this.data[id]) return;
		this.data[id] = {};
		this.emit("changed"); //Go about a normal change
		return this.updateIndex(); //But update index early so sync code works
	}

	update(id, method, pointer) { //update data
		let obj = this.data[id];
		if (!obj) return null;

		let results;
		switch(method) {
		case "add": results = OUtil.CellAdd(obj, pointer.deRef(), 0); break; //Add to the data
		case "decrease": results = OUtil.CellDecrease(obj, pointer.deRef(), 0); break; //Decrease from the data
		case "replace": results = OUtil.CellReplace(obj, pointer.deRef(), 0); break; //Replace an element of the data
		case "delete": results = OUtil.CellDelete(obj, pointer.deRef(), 0); break; //Remove an element from data
		case "clear": results = {}; //Clear a top level object
		}

		if (results) {
			this.emit("changed");
			this.data[id] = results;
			return true;
		} else {
			//Bad operation
			return null;
		}
	}

	move(id, o, nid, no) {

		if (id === nid) return this.internalMove(id, o, no);

		//First we locate the data we need to move
		const iObj = this.data[id];
		const nObj = this.data[nid];
		if (!iObj) return null; //Object not found
		if (!nObj) return null; //Object not found

		const item = OUtil.locate(iObj, o.deRef(), 0);
		if (!item) return null; //Unable To Locate
		no.assign(item); //Add value to options

		//Try to add item to new location
		const results =
			OUtil.CellType(nObj, no.deRef(), 0) === "number" ?
			OUtil.CellAdd(nObj, no.deRef(), 0) :
			OUtil.CellReplace(nObj, no.deRef(), 0);

		if (!results) {
			return null; //Failed to add item
		} else {
			const finalized =
				OUtil.CellType(iObj, o.deRef(), 0) === "number" ?
				OUtil.CellDecrease(iObj, o.deRef(), 0) :
				OUtil.CellDelete(iObj, o.deRef(), 0);

			if (!finalized) {
				return null; //Failed to remove
			} else {
				return [finalized,results]; //Send updated objects back to caller
			}
		}
	}

	internalMove(id, o, no) {
		//Only used to move data inside the same top-level
		const obj = this.data[id];
		if (!obj) return null; //Object not found

		const item = OUtil.locate(obj, o.deRef(), 0);
		if (!item) return null; //Unable To Locate
		no.assign(item); //Add value to options

		//Try to add item to new location
		let results;
		if (OUtil.CellType(obj, no.deRef(), 0) === "number" && OUtil.CellType(obj, o.deRef(), 0) === "number") {
			results = OUtil.CellDecrease(
				OUtil.CellAdd( //Nested Cell Add returns object
					obj,
					no.deRef(),
					0
				),
				o.deRef(),
				0
			);
		} else {
			results = OUtil.CellDelete(
				OUtil.CellReplace( //Nested Cell Replace returns object
					obj,
					no.deRef(),
					0
				),
				o.deRef(),
				0
			);
		}

		if (!results) {
			return null; //Failed
		} else {
			return [results, null];
		}
	}

	preSplit() {
		//We're going to need to prepare a split, so we need to figure out
		//what we're getting rid of
		const group1 = new Set();
		const group2 = new Set();
		let bool;

		//Generate 2 base groups
		for (const i in this.index) {
			bool = Math.round(Math.random()) === 1 ? true : false;
			if (bool) {
				group1.add(this.index[i]);
			} else {
				group2.add(this.index[i]);
			}
		}

		//Try to make them as close to equal as possible
		bool = true;
		let s1, //Sum of group1
			s2, //Sum of group2
			selected; //holder
		while(bool) {

			//Calculate Sums
			s1 = 0;
			s2 = 0;
			for (const i of group1) s1 += i.size;
			for (const i of group2) s2 += i.size;

			//Compare Sums
			selected = null;
			if (s1 > s2) {
				//Group1 is bigger than group2
				//Try to find something from group1
				//To move to group2
				for (const i of group1) {
					if (i.size + s2 <= s1 - i.size) {
						//This is a valid swap choice
						//Lets compare it to the currently
						//Selected choice and see if its
						//Better or worse.
						if (selected) {
							if (Math.abs((s1 - i.size) - (i.size + s2)) < Math.abs((s1 - selected.size) - (selected.size + s2))) {
								//This value is more optimal than the currently selected value
								//So this will now become selected and the loop will continue
								selected = i;
							}
						} else {
							//Nothing has been selected yet
							//Set this as selected
							selected = i;
						}
					}
				}

				//Check to see if a suitable swap value was found
				if (selected) {
					//We found a value, lets swap it
					group2.add(selected);
					group1.delete(selected);
				} else {
					//We can't optimize any further
					bool = false;
				}
			} else if (s2 > s1) {
				//Group2 is bigger than group1
				for (const i of group2) {
					if (i.size + s1 <= s2 - i.size) {
						//This is a valid swap choice
						//Lets compare it to the currently
						//Selected choice and see if its
						//Better or worse.
						if (selected) {
							if (Math.abs((s2 - i.size) - (i.size + s1)) < Math.abs((s2 - selected.size) - (selected.size + s1))) {
								//This value is more optimal than the currently selected value
								//So this will now become selected and the loop will continue
								selected = i;
							}
						} else {
							//Nothing has been selected yet
							//Set this as selected
							selected = i;
						}
					}
				}

				//Check to see if a suitable swap value was found
				if (selected) {
					//We found a value, lets swap it
					group2.delete(selected);
					group1.add(selected);
				} else {
					//We can't optimize any further
					bool = false;
				}
			} else {
				//They're equal in size
				//This should never happen
				//But it did so exit the loop
				bool = false;
			}
		}
		this.retainer = {};
		this.removed = {};
		for (const i of group1) {
			this.retainer[i.id] = i;
		}
		for (const i of group2) {
			this.removed[i.id] = i;
		}
		return this.removed;
	}

	runSplit() {
		//ONLY RUN THIS AFTER THE NEW CELL HAS FINISHED ABSORBING
		//OTHERWISE DATA WILL BE LOST - YOU HAVE BEEN WARNED!
		if (!this.retainer || !this.removed) {
			//Cell split failed, this is now a dead cell
			//So that data isn't lost
			this.die("Failed Cell Split");
		}
		this.index = this.retainer;

		//Move retained data to newData
		let newData = {};
		for (const i in this.index) {
			if (this.data[this.index[i].id]) newData[this.index[i].id] = this.data[this.index[i].id];
		}

		//Finalize
		this.data = newData;
		this.emit("changed");
	}

	write() {
		this.changed = false;
		return msgpack.encode(this.data);
	}

	die(err) {
		//This will create a dead cell
		this.emit("death", err);
	}

	absorb(indexData, callback) {

		//Asorbing data from another cell, lets get it
		let newData = {};
		const tasks = [];
		for (const i in indexData) {
			tasks.push(new Promise((resolve, reject) => {
				let timer = setTimeout(() => {
					console.log("Cell Data request left unanswered - " + this.id + " | requested " + indexData[i].id + " From " + indexData[i].cell);
					resolve([indexData[i].id, {}]);
				}, 8 * 1000);
				this.on(`return${indexData[i].id}`, (data) => {
					clearTimeout(timer);
					resolve([indexData[i].id, data]);
				});
				this.emit("request", indexData[i]);
			}).catch((err) => {
				console.log(err);
			}));
		}

		Promise.all(tasks).then((requested) => {

			//Build New data Object
			for (const [id, rdata] of requested) {
				newData[id] = rdata;
			}

			//Update Index Data object
			for (const i in indexData) {
				indexData[i].cell = this.id;
				this.removeAllListeners(`return${indexData[i].id}`);
			}

			Object.assign(this.data, newData);
			if (!this.index) this.index = {};
			Object.assign(this.index, indexData);
			this.emit("changed");
			return callback();
		}).catch((err) => {
			console.log(err)
		});
	}
}

class DeadCell {
	constructor(id, bin, data, error) {
		this.id = id;
		this.bin = bin || null;
		this.data = data || null;
		this.error = error;
	}

	cleanIndex() {
		if (!this.data) return null;
		return Object.keys(this.data);
	}
}

const Abstractor = module.exports = class extends require("events") {
	constructor() {
		super();
		console.log("Abstractor Spawned")

		this.cells = new Map();
		this.deadCells = new Map();
		this.index = {};
		this.a = 0; //Actions since last write
		this.b = Date.now(); //Last Write time
		this.active = null;
		this.root = null;
	}

	init(dir) {
		console.log("initializing...");

		//Start building cells
		this.root = dir;
		Test.loadMsgpack(dir, (id, bin) => {
			console.log(`Loading ${id}`);
			this.decode(id,bin);
		});

		//If 5 seconds pass and no cells have been loaded
		//Then this will create an empty cell
		setTimeout(() => {
			console.log("Loading Window expired");
			if (this.cells.size === 0) {
				console.log("No Cells loaded. Spawn First Cell");
				this.cells.set(0, this.spawnCell(0, {}));
			}
			this.active = true;
			console.log("Emitting Ready...");
			this.emit("ready");
			this.run();
		}, 5 * 1000);
	}

	run() {
		//This is mostly how the database maintains itself
		//In the background via self-upkeep tasks
		console.log(`Run Fired | a: ${this.a}`);
		if (this.a >= 30 || Date.now() - this.b >= 10 * 1000) {
			//Write card files if its been 30 actions or 2 minutes since
			//last write, to avoid as minimal data loss as possible in
			//case the server shuts down

			for (const [id, cell] of this.cells) {
				//Only bother to write cells that have been changed
				if (cell.changed) {
					this.write(id, cell.write());
				}
			}

			this.a = 0;
			this.b = Date.now();
		}

		setTimeout(() => {
			this.run();
		}, 1 * 1000);
	}

	decode(id, bin) {

		console.log(`Decoding ${id}`);
		//Decode msgpack binary
		let data, err;

		try {
			data = msgpack.decode(bin);
		} catch (e) {
			console.log("error decoding " + id);
			console.log(e);
			data = null;
			err = e;
		}

		if (data) {
			//Valid data - creating Cell
			const c = this.spawnCell(id, data);

			//Add this to the Cell Set
			this.cells.set(id, c);
			Object.assign(this.index, c.updateIndex());
		} else {
			//Couldnt decode data, creating dead cell
			const d = new DeadCell(id, bin, null, err);
			this.deadCells.set(id, d);
		}
	}

	divide(cell) {
		//A cell is being forced to divide
		const splitObjects = cell.preSplit();
		let splitSize = 0;
		for (const i in splitObjects) {
			splitSize += splitObjects[i].size;
		}

		//Look for a cell that can accomodate half of this cell's data
		let selected = null;
		for (const [id, obj] of this.cells) {
			if (obj.dataSize + splitSize < (MAX_SIZE * .75)) {
				selected = id;
				break;
			}
		}

		let c;
		if (selected) {
			//We found a new cell to send data to
			c = this.cells.get(selected);
		} else {
			//Spawn a new cell to send the data to
			c = this.spawnCell(this.cells.size, {});
			this.cells.set(c.id, c);
		}

		c.absorb(splitObjects, () => {
			cell.runSplit();
			Object.assign(this.index, c.updateIndex());
			Object.assign(this.index, cell.updateIndex());
		});
	}

	spawnCell(id, data) {
		console.log("SPAWNING CELL: " + id);
		const c = new Cell(id, data);

		//Listen for when a cell has divided
		c.on("divide", () => {
			this.divide(c);
		});

		//Listen for data requests
		c.on("request", (item) => {
			let rdata = this.grab(item);
			if (rdata) c.emit(`return${item.id}`, rdata);
		});

		//Listen for cell changes
		c.on("changed", () => {
			c.changed = true;
			this.a++;
			Object.assign(this.index, c.updateIndex());
		});

		//Listen for death
		c.on("death", (err) => {
			const dead = new DeadCell(c.id, null, c.data, err);

			//Clean Index
			const deIndex = dead.cleanIndex();
			if (deIndex && deIndex.length >= 1) {
				for (const i of deIndex) {
					delete this.index[i];
				}
			}

			//Update Cell Sets
			this.deadCells.set(c.id, dead);
			this.cells.delete(c.id);
		});

		return c;
	}

	getSmallestCell() {
		let selected = null;
		for (const [id, cell] of this.cells) {
			if (selected) {
				if (selected.dataSize > cell.dataSize) selected = cell;
			} else {
				selected = cell;
			}
		}

		return selected;
	}

	grab(index) {
		//Item should be an index entry
		const id = index.cell;
		const target = this.cells.get(id);
		if (target.data[index.id]) {
			return target.data[index.id];
		} else {
			return null;
		}
	}

	spawnTopLevel(id) {
		const c = this.getSmallestCell();
		Object.assign(this.index, c.addTopLevel(id));
		return {};
	}

	request(id) {
		if (this.index[id]) {
			//We have it
			let data = this.grab(this.index[id]);
			if (!data) {
				data = this.spawnTopLevel(id);
			}
			return data;
		} else {
			//We don't have it
			return null;
		}
	}

	update(id, method, options) {
		if (!this.index[id]) this.spawnTopLevel(id);
		return this.cells.get(this.index[id].cell).update(id, method, new Pointer(...options));
	}

	move(id, o, nid, no) {
		if (!this.index[id]) {
			//Create a new top level object, but return null
			//because we wont be able to move
			this.spawnTopLevel(id);
			return null;
		}
		if (!this.index[nid]) {
			this.spawnTopLevel(nid);
		}

		let results;
		if (this.index[id].cell === this.index[nid].cell) {
			//IntraCell Move
			results = this.cells.get(this.index[id].cell).move(id, new Pointer(...o), nid, new Pointer(...no));

			let cell;
			if (results && results[1]) {
				//Internal Move
				cell = this.cells.get(this.index[id].cell);
				cell.data[id] = results[0];
				cell.emit("changed");
				this.cells.set(this.index[id].cell, cell);
				return true;
			} else if (results) {
				//Normal Move
				cell = this.cells.get(this.index[id].cell);
				cell.data[id] = results[0];
				cell.data[nid] = results[1];
				cell.emit("changed");
				this.cells.set(this.index[id].cell, cell);
				return true;
			} else {
				//Failure
				return null;
			}
		} else {
			//InterCell move
			const iCell = this.cells.get(this.index[id].cell);
			const nCell = this.cells.get(this.index[nid].cell);
			results = OUtil.move(iCell, id, new Pointer(...o), nCell, nid, new Pointer(...no));

			if (results) {
				iCell.data[id] = results[0];
				nCell.data[nid] = results[1];
				iCell.emit("changed");
				nCell.emit("changed");
				this.cells.set(this.index[id].cell, iCell);
				this.cells.set(this.index[nid].cell, nCell);
				return true;
			} else {
				return null;
			}
		}
	}

	merge(id, nid, options) {
		//Merge 2 top level objects
		if (!this.index[nid]) this.spawnTopLevel(nid);
		if (!this.index[id]) {
			//Spawn top level, but return true because merging is pointless
			this.spawnTopLevel(id);
			return true;
		}

		if (this.index[id].cell === this.index[nid].cell) {
			//IntraCell merge
			const c = this.cells.get(this.index[id].cell);

			c.data[nid] = OUtil.merge(c.data[id], c.data[nid], options);
			c.data[id] = {};

			this.cells.set(id, c);
			return true;
		} else {
			//InterCell merge
			const iCell = this.cells.get(this.index[id].cell);
			const nCell = this.cells.get(this.index[nid].cell);

			nCell.data[nid] = OUtil.merge(iCell.data[id], nCell.data[nid], options);
			iCell.data[id] = {};

			this.cells.set(id, iCell);
			this.cells.set(nid, nCell);
			return true;
		}
	}

	write(id, encoded) {
		Test.writeMsgpack(`${this.root}/${id}`, encoded, (err) => {
			if (err) console.log(err);
		});
	}
}