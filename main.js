"use strict";

function toId(str) {
	return str.toLowerCase().replace(/[^a-z0-9]+/g, "");
}

let op = null;
for (let i = 0; i < process.argv.length; i++) {
	if (i < 2) continue;
	op = process.argv[i];
	break;
}

if (!op) {
	console.log("No Test Specified; Exiting...");
	process.exit();
} else {
	op = toId(op);
}

if (!require.resolve(`./test/${op}`)) {
	const options = require("fs").readdirSync("test");
	if (options && options.length > 0) {
		console.log("Invalid Test id: " + op);
		console.log("Valid Options: " + options.join(", "));
	} else {
		console.log("No Tests Available...");
	}
	process.exit();
}

const Abstractor = require("./abstractor");
const path = require("path")

class Main {
	constructor(script) {
		this.script = script;
	}

	run() {
		this.script.start(Abstractor);
	}

	loadMsgpack(dir, callback) {
		console.log(`Loading msgpack dir ${dir}`);
		require("fs").readdirSync(`${dir}`).filter((file) => {
			return path.extname(file) === ".mp";
		}).forEach((file) => {
			callback(file.replace(".mp", ""), require("fs").readFileSync(dir + "/" + file));
		});
	}

	writeMsgpack(link, encoded, callback) {
		//Assumes pre-encoded data
		let error = null;
		try {
			require("fs").writeFileSync(`${link}.mp`, encoded);
		} catch (e) {
			error = e;
		}

		return callback(error);
	}
}

global.Test = new Main(require(`./test/${op}`));
Test.run();